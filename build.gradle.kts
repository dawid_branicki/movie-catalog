import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.4.5"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.4.32"
	kotlin("plugin.spring") version "1.4.32"
	id("nu.studer.jooq") version "5.2.1"
	id("org.liquibase.gradle") version "2.0.4"
	id("com.avast.gradle.docker-compose") version "0.14.3"
}

java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-jooq")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.liquibase:liquibase-core")
	implementation("io.springfox:springfox-swagger-ui:3.0.0")
	implementation("io.springfox:springfox-boot-starter:3.0.0")
	implementation("org.apache.httpcomponents:httpclient:4.5.13")
	implementation("net.logstash.logback:logstash-logback-encoder:6.3")
	implementation("ch.qos.logback:logback-classic")
	runtimeOnly("org.postgresql:postgresql")
	jooqGenerator("org.postgresql:postgresql")
	liquibaseRuntime("org.postgresql:postgresql")
	liquibaseRuntime("org.liquibase:liquibase-core")
	liquibaseRuntime("ch.qos.logback:logback-core:1.2.3")
	liquibaseRuntime("ch.qos.logback:logback-classic:1.2.3")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.23")
	testImplementation("io.mockk:mockk:1.10.5")
	testImplementation("io.kotest:kotest-assertions-core-jvm:4.5.0")
	testImplementation("com.github.tomakehurst:wiremock:2.24.1")
	testImplementation("ru.lanwen.wiremock:wiremock-junit5:1.3.1")
	testImplementation("org.testcontainers:postgresql:1.15.2")
}

val profile: String? by project
tasks.bootRun {
	main = "com.demo.moviecatalog.MovieCatalogApplicationKt"
	doFirst {
		requireNotNull(profile) { "Please supply run profile. -Pprofile=name " }
		args("--spring.profiles.active=${profile}")
	}
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf(
			"-Xjsr305=strict",
			"-Xopt-in=kotlin.RequiresOptIn"
		)
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

@Suppress("UnstableApiUsage")
val processResources by tasks.getting(ProcessResources::class) {
	filesMatching("application.yml") {
		expand(project.properties)
	}
}

jooq {
	version.set("3.14.7")

	configurations {
		create("main") {
			generateSchemaSourceOnCompilation.set(true)
			jooqConfiguration.apply {
				generator.apply {
					name = "org.jooq.codegen.KotlinGenerator"
					jdbc.apply {
						driver = "org.postgresql.Driver"
						url = "jdbc:postgresql://localhost:5433/jooq"
						password = "jooq"
						user = "jooq"
					}
					database.apply {
						name = "org.jooq.meta.postgres.PostgresDatabase"
						inputSchema = "movie_catalog"
					}
					generate.apply {
						isDeprecated = false
						isRecords = true
						isImmutablePojos = false
						isFluentSetters = false
						isJavaTimeTypes = true
					}
					target.apply {
						packageName = "${project.group}.jooq"
						directory = "build/generated/jooq/main"
					}
					strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
				}
			}
		}
	}
}

liquibase {
	activities.register("main") {
		arguments = mapOf(
			"logLevel" to "warn",
			"changeLogFile" to "src/main/resources/db/db-changelog.xml",
			"url" to "jdbc:postgresql://localhost:5433/jooq",
			"username" to "jooq",
			"password" to "jooq"
		)
	}
}

dockerCompose {
	useComposeFiles = listOf("docker-compose-jooq.yml")
	startedServices = listOf("postgres")
}

tasks.named("generateJooq") {
	dependsOn(":update")
	finalizedBy(":composeDown")
}

tasks.named("update") {
	dependsOn(":composeUp")
}