package com.demo.moviecatalog.infrastructure

import com.demo.type.IMDbId
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.equalTo
import io.kotest.assertions.asClue
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.core.io.ClassPathResource
import ru.lanwen.wiremock.config.WiremockCustomizer
import ru.lanwen.wiremock.ext.WiremockResolver
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock
import ru.lanwen.wiremock.ext.WiremockUriResolver
import ru.lanwen.wiremock.ext.WiremockUriResolver.WiremockUri

@ExtendWith(
	value = [
		WiremockResolver::class,
		WiremockUriResolver::class
	]
)
internal class ExternalOpenMovieRepositoryTest {

	@Test
	fun `should successfully find move in the external repository`(
		@Wiremock(customizer = ExternalOpenMovieApiEndpoint::class) server: WireMockServer,
		@WiremockUri uri: String
	) {
		//given
		val apiKey = "123"
		val repository = ExternalOpenMovieRepository(apiKey, uri)
		val imDbId = IMDbId("tt0232500")

		//when
		val result = repository.findByIMBdId(imDbId)

		//then
		result.shouldNotBeNull().asClue {
			it.imdbId shouldBe imDbId
			it.ratings shouldHaveSize 3
			it.details.runtime shouldBe "106 min"
		}
	}

	@Test
	fun `should not find move in the external repository`(
		@Wiremock(customizer = ExternalOpenMovieApiEndpoint::class) server: WireMockServer,
		@WiremockUri uri: String
	) {
		//given
		val apiKey = "123"
		val repository = ExternalOpenMovieRepository(apiKey, uri)
		val imDbId = IMDbId("tt02325000")

		//when
		val result = repository.findByIMBdId(imDbId)

		//then
		result.shouldBeNull()
	}
}

class ExternalOpenMovieApiEndpoint : WiremockCustomizer {
	override fun customize(server: WireMockServer) {
		val data = ClassPathResource("tt0232500.json").file.readText()
		server.stubFor(
			WireMock.get(WireMock.anyUrl())
				.withQueryParam("apiKey", equalTo("123"))
				.withQueryParam("i", equalTo("tt0232500"))
				.willReturn(WireMock.aResponse().withBody(data))
		)
	}
}