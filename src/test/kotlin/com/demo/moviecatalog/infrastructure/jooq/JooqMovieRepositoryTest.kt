package com.demo.moviecatalog.infrastructure.jooq

import com.demo.moviecatalog.domain.Movie
import com.demo.moviecatalog.domain.MovieDetails
import com.demo.type.IMDbId
import com.demo.type.USD
import com.demo.util.Page
import io.kotest.assertions.asClue
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class JooqMovieRepositoryTest {

	@Autowired
	private lateinit var jooqMovieRepository: JooqMovieRepository

	@AfterEach
	fun teardown() {
		jooqMovieRepository.deleteAll()
	}

	@Test
	fun `should save movie and find`() {
		//given
		val movie = Movie(
			imdbId = IMDbId("test"),
			details = MovieDetails("", 2021, "", "", "", "", "", ""),
			ratings = emptyList()
		)

		//when
		jooqMovieRepository.save(movie)
		val result = jooqMovieRepository.findMovie(movie.id)

		//then
		result.shouldNotBeNull()
	}

	@Test
	fun `should update movie and find`() {
		//given
		val movie = Movie(
			imdbId = IMDbId("test"),
			details = MovieDetails("", 2021, "", "", "", "", "", ""),
			ratings = emptyList()
		)
		jooqMovieRepository.save(movie)
		movie.changePrice(USD(20.00), USD(10.00))

		//when
		jooqMovieRepository.save(movie)
		val result = jooqMovieRepository.findMovie(movie.id)

		//then
		result.shouldNotBeNull().asClue {
			it.price.regular shouldBe USD(20.00)
			it.price.half shouldBe USD(10.00)
		}
	}

	@Test
	fun `should return movies with paging`() {
		//given
		(1..10)
			.map {
				Movie(
					imdbId = IMDbId("test-${it}"),
					details = MovieDetails("", 2021, "", "", "", "", "", ""),
					ratings = emptyList()
				)
			}.forEach { jooqMovieRepository.save(it) }

		//when
		val firstSearch = jooqMovieRepository.getMovies(Page(1, 8))
		val secondSearch = jooqMovieRepository.getMovies(Page(2, 8))

		//then
		firstSearch.asClue {
			it.totalItems shouldBe 10
			it.items shouldHaveSize 8
		}
		secondSearch.asClue {
			it.totalItems shouldBe 10
			it.items shouldHaveSize 2
		}
	}
}