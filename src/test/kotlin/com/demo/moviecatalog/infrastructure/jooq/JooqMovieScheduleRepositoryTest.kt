package com.demo.moviecatalog.infrastructure.jooq

import assertk.assertThat
import assertk.assertions.isFailure
import assertk.assertions.isInstanceOf
import com.demo.moviecatalog.domain.Movie
import com.demo.moviecatalog.domain.MovieDetails
import com.demo.moviecatalog.domain.MovieSchedule
import com.demo.moviecatalog.domain.MovieScheduleDate
import com.demo.type.IMDbId
import com.demo.type.MovieId
import io.kotest.matchers.collections.shouldContainInOrder
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class JooqMovieScheduleRepositoryTest {

	@Autowired
	private lateinit var jooqMovieScheduleRepository: JooqMovieScheduleRepository

	@Autowired
	private lateinit var jooqMovieRepository: JooqMovieRepository

	@AfterEach
	fun teardown() {
		jooqMovieRepository.deleteAll()
	}

	@Test
	fun `should save and find schedule`() {
		//given
		val movie = Movie(
			imdbId = IMDbId("test"),
			details = MovieDetails("", 2021, "", "", "", "", "", ""),
			ratings = emptyList()
		)
		val schedule = MovieSchedule(date = MovieScheduleDate(2021, 5), movieId = movie.id)
		jooqMovieRepository.save(movie)

		//when
		jooqMovieScheduleRepository.save(schedule)
		val result = jooqMovieScheduleRepository.findById(schedule.id)

		//then
		result.shouldNotBeNull()
	}

	@Test
	fun `should fail try save and find schedule when movie not exists`() {
		//given
		val schedule = MovieSchedule(date = MovieScheduleDate(2021, 5), movieId = MovieId())

		//when
		val result = assertThat {
			jooqMovieScheduleRepository.save(schedule)
		}

		//then
		result.isFailure().isInstanceOf(DataIntegrityViolationException::class)
	}

	@Test
	fun `should return schedules in correct order`() {
		//given
		val movie = Movie(
			imdbId = IMDbId("test"),
			details = MovieDetails("", 2021, "", "", "", "", "", ""),
			ratings = emptyList()
		)
		val schedule1 = MovieSchedule(date = MovieScheduleDate(2021, 8), movieId = movie.id)
		val schedule2 = MovieSchedule(date = MovieScheduleDate(2021, 5), movieId = movie.id)
		val schedule3 = MovieSchedule(date = MovieScheduleDate(2021, 11), movieId = movie.id)

		jooqMovieRepository.save(movie)
		jooqMovieScheduleRepository.save(schedule1)
		jooqMovieScheduleRepository.save(schedule2)
		jooqMovieScheduleRepository.save(schedule3)

		//when
		val result = jooqMovieScheduleRepository.getMovieSchedules(movie.id)

		//then
		result.should {
			it shouldHaveSize 3
			it.map(MovieSchedule::id) shouldContainInOrder listOf(schedule2.id, schedule1.id, schedule3.id)
		}
	}

}