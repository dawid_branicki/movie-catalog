package com.demo.moviecatalog.domain

import com.demo.type.ReviewNote
import com.demo.type.ReviewNoteNotInRangeException
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class ReviewNoteTest {

	@Test
	fun `should throw exception when note not in range`() {
		//given + when
		val exception = shouldThrow<ReviewNoteNotInRangeException> {
			ReviewNote(6)
		}

		//then
		exception.message shouldBe "Review note must be in range 1-5. Current value equals 6"
	}

	@Test
	fun `should not throw exception when note in range`() {
		//given + when
		val result = shouldNotThrowAny {
			ReviewNote(5)
		}

		//then
		result.value shouldBe 5
	}
}