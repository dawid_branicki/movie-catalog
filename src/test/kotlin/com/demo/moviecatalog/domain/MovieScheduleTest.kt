package com.demo.moviecatalog.domain

import com.demo.type.MovieId
import com.demo.type.MovieTimeSlotId
import io.kotest.assertions.throwables.shouldThrowAny
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.throwable.shouldHaveMessage
import io.kotest.matchers.types.beInstanceOf
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

internal class MovieScheduleTest {

	@Test
	fun `should successfully add time slot`() {
		//given
		val schedule = MovieSchedule(date = MovieScheduleDate(2021, 5), movieId = MovieId())
		val timeSlot = MovieTimeSlot(
			start = SlotDateTime(LocalDateTime.of(2021, 5, 2, 10, 0)),
			end = SlotDateTime(LocalDateTime.of(2021, 5, 2, 12, 0))
		)

		//then
		schedule[MovieTimeSlotId()] = timeSlot

		//then
		schedule.size shouldBe 1
	}

	@Test
	fun `should throw exception when time slot not in schedule date range`() {
		//given
		val schedule = MovieSchedule(date = MovieScheduleDate(2021, 5), movieId = MovieId())
		val timeSlot = MovieTimeSlot(
			start = SlotDateTime(LocalDateTime.of(2021, 6, 1, 10, 0)),
			end = SlotDateTime(LocalDateTime.of(2021, 6, 1, 12, 0))
		)

		//then
		val exception = shouldThrowAny {
			schedule.put(MovieTimeSlotId(), timeSlot)
		}

		//then
		exception should beInstanceOf<MovieTimeSlotNotInScheduleRangeException>()
		exception shouldHaveMessage "Movie time slot is not in schedule date range"
	}

	@Test
	fun `should throw exception when time slots conflict`() {
		//given
		val schedule = MovieSchedule(date = MovieScheduleDate(2021, 5), movieId = MovieId())
		val timeSlot1 = MovieTimeSlot(
			start = SlotDateTime(LocalDateTime.of(2021, 5, 1, 10, 0)),
			end = SlotDateTime(LocalDateTime.of(2021, 5, 1, 12, 0))
		)
		val timeSlot2 = MovieTimeSlot(
			start = SlotDateTime(LocalDateTime.of(2021, 5, 1, 11, 45)),
			end = SlotDateTime(LocalDateTime.of(2021, 5, 1, 13, 45))
		)

		//then
		schedule[MovieTimeSlotId()] = timeSlot1
		val exception = shouldThrowAny {
			schedule.put(MovieTimeSlotId(), timeSlot2)
		}

		//then
		exception should beInstanceOf<MovieTimeSlotConflictException>()
	}
}