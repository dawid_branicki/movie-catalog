package com.demo.moviecatalog.application

import com.demo.moviecatalog.domain.*
import com.demo.type.IMDbId
import com.demo.type.ReviewNote
import com.demo.type.USD
import io.kotest.assertions.asClue
import io.kotest.assertions.throwables.shouldThrowAny
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.instanceOf
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class MovieFacadeTest {

	private lateinit var movieFacade: MovieFacade
	private val movieRepository: MovieRepository = mockk()
	private val openMovieRepository: OpenMovieRepository = mockk()

	@BeforeEach
	fun setUp() {
		movieFacade = MovieFacade(movieRepository, openMovieRepository)
	}

	@Test
	fun `should create movie`() {
		//given
		val imDbId = IMDbId("123ffz")
		val movie = Movie(imdbId = imDbId, details = MovieDetails("title", 2021, "", "", "", "", "", ""), ratings = emptyList())
		val slot = slot<Movie>()
		every { openMovieRepository.findByIMBdId(any()) } returns movie
		every { movieRepository.save(capture(slot)) } just runs

		//when
		movieFacade.createMovie(imDbId)

		//then
		verify(exactly = 1) { movieRepository.save(any()) }
		slot.captured.asClue {
			it.isActive shouldBe false
			it.price.half shouldBe USD.ZERO
			it.price.regular shouldBe USD.ZERO
		}
	}

	@Test
	fun `should throw exception when create movie but movie not found in open api`() {
		//given
		val imDbId = IMDbId("123ffz")
		every { openMovieRepository.findByIMBdId(any()) } returns null

		//when
		val exception = shouldThrowAny { movieFacade.createMovie(imDbId) }

		//then
		exception shouldBe instanceOf<MovieNotFoundInOpenRepositoryException>()
	}

	@Test
	fun `should mark movie as active`() {
		//given
		val movie = Movie(imdbId = IMDbId("123ffz"), details = MovieDetails("title", 2021, "", "", "", "", "", ""), ratings = emptyList())
		val slot = slot<Movie>()
		every { movieRepository.findMovie(any()) } returns movie
		every { movieRepository.save(capture(slot)) } just runs

		//when
		movieFacade.markMovieAsActive(movie.id)

		//then
		slot.captured.isActive shouldBe true
	}

	@Test
	fun `should mark movie as inactive`() {
		//given
		val movie = Movie(isActive = true, imdbId = IMDbId("123ffz"), details = MovieDetails("title", 2021, "", "", "", "", "", ""), ratings = emptyList())
		val slot = slot<Movie>()
		every { movieRepository.findMovie(any()) } returns movie
		every { movieRepository.save(capture(slot)) } just runs

		//when
		movieFacade.markMoveAsInactive(movie.id)

		//then
		slot.captured.isActive shouldBe false
	}

	@Test
	fun `should edit movie`() {
		//given
		val command: EditMovieCommand = mockk {
			every { regularPrice } returns USD(8.00)
			every { halfPrice } returns USD(6.00)
		}
		val movie = Movie(isActive = true, imdbId = IMDbId("123ffz"), details = MovieDetails("title", 2021, "", "", "", "", "", ""), ratings = emptyList())
		val slot = slot<Movie>()
		every { movieRepository.findMovie(any()) } returns movie
		every { movieRepository.save(capture(slot)) } just runs

		//when
		movieFacade.editMovie(movie.id, command)

		//then
		slot.captured.asClue {
			it.price.regular shouldBe USD(8.00)
			it.price.half shouldBe USD(6.00)
		}
	}

	@Test
	fun `should add movie review`() {
		//given
		val command: AddMovieReviewCommand = mockk {
			every { note } returns ReviewNote(4)
		}
		val movie = Movie(isActive = true, imdbId = IMDbId("123ffz"), details = MovieDetails("title", 2021, "", "", "", "", "", ""), ratings = emptyList())
		val slot = slot<Movie>()
		every { movieRepository.findMovie(any()) } returns movie
		every { movieRepository.save(capture(slot)) } just runs

		//when
		movieFacade.addMovieReview(movie.id, command)

		//then
		slot.captured.asClue {
			it.reviewRating.value shouldBe "4.00".toBigDecimal()
		}
	}
}