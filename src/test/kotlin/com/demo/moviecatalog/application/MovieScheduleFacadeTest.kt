package com.demo.moviecatalog.application

import com.demo.moviecatalog.domain.*
import com.demo.type.MovieId
import com.demo.type.MovieScheduleId
import com.demo.type.MovieTimeSlotId
import io.kotest.assertions.asClue
import io.kotest.assertions.throwables.shouldThrowAny
import io.kotest.matchers.maps.shouldHaveSize
import io.kotest.matchers.maps.shouldNotContainKey
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.instanceOf
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

internal class MovieScheduleFacadeTest {

	private lateinit var movieScheduleFacade: MovieScheduleFacade
	private val movieScheduleRepository: MovieScheduleRepository = mockk()

	@BeforeEach
	fun setUp() {
		movieScheduleFacade = MovieScheduleFacade(movieScheduleRepository)
	}

	@Test
	fun `should create new schedule`() {
		//given
		val command: NewMovieScheduleCommand = mockk {
			every { movieId } returns MovieId()
			every { year } returns 2021
			every { month } returns 5
		}
		val slot = slot<MovieSchedule>()
		every { movieScheduleRepository.save(capture(slot)) } just runs

		//when
		movieScheduleFacade.creteNewMovieSchedule(command)

		//then
		verify(exactly = 1) { movieScheduleRepository.save(any()) }
		slot.captured.asClue {
			it.movieId shouldBe command.movieId
			it.date shouldBe MovieScheduleDate(2021, 5)
		}
	}

	@Test
	fun `should add new time slot to movie schedule`() {
		//given
		val movieId = MovieId()
		val movieSchedule = MovieSchedule(movieId = movieId, date = MovieScheduleDate(2021, 5))
		val command: AddMovieTimeSlotsCommand = mockk {
			every { slots } returns listOf(mockk {
				every { start } returns LocalDateTime.of(2021, 5, 2, 10, 0)
				every { end } returns LocalDateTime.of(2021, 5, 2, 12, 0)
			})
		}
		val slot = slot<MovieSchedule>()
		every { movieScheduleRepository.findById(any()) } returns movieSchedule
		every { movieScheduleRepository.save(capture(slot)) } just runs

		//when
		movieScheduleFacade.addMovieTimeSlots(movieSchedule.id, command)

		//then
		verify(exactly = 1) { movieScheduleRepository.save(any()) }
		slot.captured.asClue {
			it.movieId shouldBe movieId
			it.date shouldBe MovieScheduleDate(2021, 5)
			it.timeSlots shouldHaveSize 1
		}
	}

	@Test
	fun `should throw exception if schedule not exists when try add new time slot`() {
		//given
		val command: AddMovieTimeSlotsCommand = mockk {
			every { slots } returns listOf(mockk {
				every { start } returns LocalDateTime.of(2021, 5, 2, 10, 0)
				every { end } returns LocalDateTime.of(2021, 5, 2, 12, 0)
			})
		}
		every { movieScheduleRepository.findById(any()) } returns null

		//when
		val exception = shouldThrowAny {
			movieScheduleFacade.addMovieTimeSlots(MovieScheduleId(), command)
		}

		//then
		verify(exactly = 0) { movieScheduleRepository.save(any()) }
		exception shouldBe instanceOf<MovieScheduleNotFoundException>()
	}

	@Test
	fun `should remove time slot from schedule`() {
		//given
		val slotId = MovieTimeSlotId()
		val movieId = MovieId()
		val movieSchedule = MovieSchedule(movieId = movieId, date = MovieScheduleDate(2021, 5))
		movieSchedule[slotId] = MovieTimeSlot(
			start = SlotDateTime(LocalDateTime.of(2021, 5, 1, 10, 0)),
			end = SlotDateTime(LocalDateTime.of(2021, 5, 1, 12, 0))
		)
		val slot = slot<MovieSchedule>()
		every { movieScheduleRepository.findById(any()) } returns movieSchedule
		every { movieScheduleRepository.save(capture(slot)) } just runs

		//when
		movieScheduleFacade.removeMovieTimeSlot(movieSchedule.id, slotId)

		//then
		slot.captured.asClue {
			it.timeSlots shouldNotContainKey slotId
			it.timeSlots shouldHaveSize 0
		}
	}
}