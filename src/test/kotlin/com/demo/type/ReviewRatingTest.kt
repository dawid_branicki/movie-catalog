package com.demo.type

import com.demo.moviecatalog.domain.ReviewRating
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

internal class ReviewRatingTest {


	@Test
	fun `should return review rating correctly`() {
		//given
		val reviewRating = ReviewRating()

		//when
		reviewRating.add(ReviewNote(5))
		reviewRating.add(ReviewNote(4))
		reviewRating.add(ReviewNote(3))
		reviewRating.add(ReviewNote(2))
		reviewRating.add(ReviewNote(1))
		reviewRating.add(ReviewNote(4))

		//then
		reviewRating.value shouldBe "3.17".toBigDecimal()
	}
}