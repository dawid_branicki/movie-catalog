package com.demo.moviecatalog.domain

import com.demo.type.IMDbId
import com.demo.type.MovieId
import com.demo.type.MovieScheduleId
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class MovieNotFoundException(movieId: MovieId) :
	RuntimeException("Movie not found. Id: ${movieId.value}")

@ResponseStatus(HttpStatus.NOT_FOUND)
class MovieNotFoundInOpenRepositoryException(imDbId: IMDbId) :
	RuntimeException("Movie not found in open repository. ImDbId: ${imDbId.value}")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class MovieTimeSlotNotInScheduleRangeException :
	RuntimeException("Movie time slot is not in schedule date range")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class MovieTimeSlotConflictException :
	RuntimeException("Movie time slot conflict")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class MovieTimeSlotRangeException :
	RuntimeException("Movie time slot date start is after end date")

@ResponseStatus(HttpStatus.NOT_FOUND)
class MovieScheduleNotFoundException(movieScheduleId: MovieScheduleId) :
	RuntimeException("Movie schedule not found. Id: ${movieScheduleId.value}")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class MovieScheduleYearNotInRangeException :
	RuntimeException("Movie schedule date year not in range")

@ResponseStatus(HttpStatus.BAD_REQUEST)
class MovieScheduleMonthNotInRangeException :
	RuntimeException("Movie schedule date month not in range")