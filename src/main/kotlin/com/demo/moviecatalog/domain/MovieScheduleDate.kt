package com.demo.moviecatalog.domain

import com.demo.util.validate
import java.time.LocalDate

data class MovieScheduleDate(
	val value: LocalDate
) {
	val month get() = value.monthValue
	val year get() = value.year

	constructor(year: Int, month: Int) : this(LocalDate.of(year, month, 1)) {
		validate(year in 2021..2100) { MovieScheduleYearNotInRangeException() }
		validate(month in 1..12) { MovieScheduleMonthNotInRangeException() }
	}
}