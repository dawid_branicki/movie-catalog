package com.demo.moviecatalog.domain

import com.demo.type.IMDbId
import com.demo.type.MovieId
import com.demo.type.ReviewNote
import com.demo.type.USD

class Movie(
	val id: MovieId = MovieId(),
	val imdbId: IMDbId,
	val details: MovieDetails,
	val ratings: List<Rating>,
	val reviewRating: ReviewRating = ReviewRating(),
	price: Price = Price(),
	isActive: Boolean = false,
) {
	var price: Price = price
		private set
	var isActive: Boolean = isActive
		private set

	fun markAsActive() {
		isActive = true
	}

	fun markAsInactive() {
		isActive = false
	}

	fun changePrice(regular: USD, half: USD) {
		price.regular = regular
		price.half = half
	}

	fun addReview(note: ReviewNote) {
		reviewRating.add(note)
	}

}