package com.demo.moviecatalog.domain

import com.demo.type.USD

class Price(
	var regular: USD = USD.ZERO,
	var half: USD = USD.ZERO,
)