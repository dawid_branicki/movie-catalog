package com.demo.moviecatalog.domain

import com.demo.type.MovieId
import com.demo.type.MovieScheduleId
import com.demo.type.MovieTimeSlotId
import com.demo.util.validate


class MovieSchedule(
	val id: MovieScheduleId = MovieScheduleId(),
	val date: MovieScheduleDate,
	val movieId: MovieId,
	val timeSlots: HashMap<MovieTimeSlotId, MovieTimeSlot> = hashMapOf()
) : MutableMap<MovieTimeSlotId, MovieTimeSlot> by timeSlots {

	override fun put(key: MovieTimeSlotId, value: MovieTimeSlot): MovieTimeSlot {
		validate(date in value) { MovieTimeSlotNotInScheduleRangeException() }
		validate(timeSlots.values.none { value in it }) { MovieTimeSlotConflictException() }

		timeSlots[key] = value
		return value
	}

}