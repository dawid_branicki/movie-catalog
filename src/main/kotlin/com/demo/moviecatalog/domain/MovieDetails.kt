package com.demo.moviecatalog.domain

class MovieDetails(
	val title: String,
	val year: Int,
	val runtime: String,
	val genre: String,
	val plot: String,
	val released: String,
	val actors: String,
	val poster: String
)