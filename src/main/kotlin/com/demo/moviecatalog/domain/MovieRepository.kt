package com.demo.moviecatalog.domain

import com.demo.type.MovieId
import com.demo.util.Page
import com.demo.util.Pager

interface MovieRepository {
	fun save(movie: Movie)
	fun findMovie(movieId: MovieId): Movie?
	fun getMovies(page: Page): Pager<Movie>
	fun getActiveMovies(page: Page): Pager<Movie>
}