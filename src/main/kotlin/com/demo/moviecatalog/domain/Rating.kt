package com.demo.moviecatalog.domain

class Rating(
	val source: String,
	val value: String
)