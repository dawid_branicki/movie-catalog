package com.demo.moviecatalog.domain

import com.demo.type.MovieId
import com.demo.type.MovieScheduleId

interface MovieScheduleRepository {
	fun save(movieSchedule: MovieSchedule)
	fun findById(movieScheduleId: MovieScheduleId): MovieSchedule?
	fun getMovieSchedules(movieId: MovieId): List<MovieSchedule>
}