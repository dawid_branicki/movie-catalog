package com.demo.moviecatalog.domain

import com.fasterxml.jackson.annotation.JsonValue
import java.time.LocalDateTime

data class SlotDateTime(
	@JsonValue val value: LocalDateTime
) : Comparable<SlotDateTime> {

	val year get() = value.year
	val month get() = value.monthValue

	override fun compareTo(other: SlotDateTime) = compareValuesBy(this, other, SlotDateTime::value)
}
