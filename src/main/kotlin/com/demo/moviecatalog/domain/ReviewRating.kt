package com.demo.moviecatalog.domain

import com.demo.type.ReviewNote
import java.math.BigDecimal
import java.math.RoundingMode

class ReviewRating(
	value: BigDecimal = BigDecimal.ZERO,
	private var currentNoteSum: Long = 0,
	private var reviewAmount: Long = 0
) {
	init {
		require(value >= BigDecimal.ZERO) { "value must be greater or equals zero" }
		require(currentNoteSum >= 0) { "current note sum must be greater or equals zero" }
		require(reviewAmount >= 0) { "review amount must be greater or equals zero" }
	}

	var value = value
		private set

	fun add(note: ReviewNote) {
		currentNoteSum = note + currentNoteSum
		reviewAmount++
		value = (currentNoteSum.toBigDecimalRound2() / reviewAmount.toBigDecimalRound2())
			.setScale(2, RoundingMode.HALF_UP)
	}

	private fun Long.toBigDecimalRound2() = this.toBigDecimal().setScale(2)
}