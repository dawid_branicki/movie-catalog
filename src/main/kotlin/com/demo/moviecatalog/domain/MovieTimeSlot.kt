package com.demo.moviecatalog.domain

import com.demo.util.validate

data class MovieTimeSlot(
	val start: SlotDateTime,
	val end: SlotDateTime
) {

	init {
		validate(end > start) { MovieTimeSlotRangeException() }
	}

	operator fun contains(date: MovieScheduleDate): Boolean {
		return date.year in start.year..end.year && date.month in start.month..end.month
	}

	operator fun contains(slot: MovieTimeSlot): Boolean {
		return slot.start in start..end || slot.end in start..end
	}
}