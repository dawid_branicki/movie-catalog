package com.demo.moviecatalog.domain

import com.demo.type.IMDbId

interface OpenMovieRepository {
	fun findByIMBdId(imDbId: IMDbId): Movie?
}