package com.demo.moviecatalog.infrastructure.jooq


import com.demo.jooq.tables.Movies.Companion.MOVIES
import com.demo.jooq.tables.records.MoviesRecord
import com.demo.moviecatalog.domain.Movie
import com.demo.moviecatalog.domain.MovieRepository
import com.demo.type.IMDbId
import com.demo.type.MovieId
import com.demo.util.Page
import com.demo.util.Pager
import com.demo.util.readAsObject
import com.demo.util.writeAsJSONB
import org.jooq.DSLContext
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class JooqMovieRepository(
	private val dslContext: DSLContext
) : MovieRepository {

	@Transactional
	override fun save(movie: Movie) {
		with(MOVIES) {
			dslContext.insertInto(this)
				.set(movie.toRecord())
				.onConflict(MOVIES.ID)
				.doUpdate()
				.set(movie.toRecord())
				.execute()
		}
	}

	@Transactional(readOnly = true)
	override fun findMovie(movieId: MovieId): Movie? {
		return dslContext.selectFrom(MOVIES)
			.where(MOVIES.ID.eq(movieId.value))
			.fetchOne { it.toEntity() }
	}

	@Transactional(readOnly = true)
	override fun getMovies(page: Page): Pager<Movie> {
		val totalItems = dslContext.fetchCount(MOVIES)
		val items = dslContext.selectFrom(MOVIES)
			.offset(page.offset)
			.limit(page.limit)
			.fetch { it.toEntity() }

		return page.createPager(totalItems, items)
	}

	@Transactional(readOnly = true)
	override fun getActiveMovies(page: Page): Pager<Movie> {
		val totalItems = dslContext.fetchCount(MOVIES, MOVIES.IS_ACTIVE.isTrue)
		val items = dslContext.selectFrom(MOVIES)
			.where(MOVIES.IS_ACTIVE.isTrue)
			.offset(page.offset)
			.limit(page.limit)
			.fetch { it.toEntity() }

		return page.createPager(totalItems, items)
	}

	@Transactional
	fun deleteAll() {
		dslContext
			.truncate(MOVIES)
			.cascade()
			.execute()
	}

	private fun Movie.toRecord() = MoviesRecord(
		id = id.value,
		imdbId = imdbId.value,
		isActive = isActive,
		details = details.writeAsJSONB(),
		ratings = ratings.writeAsJSONB(),
		price = price.writeAsJSONB(),
		reviewRating = reviewRating.writeAsJSONB()
	)

	private fun MoviesRecord.toEntity() = Movie(
		id = MovieId(id!!),
		isActive = isActive!!,
		details = details!!.readAsObject(),
		imdbId = IMDbId(imdbId!!),
		ratings = ratings!!.readAsObject(),
		price = price!!.readAsObject(),
		reviewRating = reviewRating!!.readAsObject()
	)
}