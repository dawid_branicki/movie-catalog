package com.demo.moviecatalog.infrastructure.jooq

import com.demo.jooq.tables.records.MovieSchedulesRecord
import com.demo.jooq.tables.references.MOVIE_SCHEDULES
import com.demo.moviecatalog.domain.MovieSchedule
import com.demo.moviecatalog.domain.MovieScheduleDate
import com.demo.moviecatalog.domain.MovieScheduleRepository
import com.demo.type.MovieId
import com.demo.type.MovieScheduleId
import com.demo.util.readAsObject
import com.demo.util.writeAsJSONB
import org.jooq.DSLContext
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class JooqMovieScheduleRepository(
	private val dslContext: DSLContext
) : MovieScheduleRepository {

	@Transactional
	override fun save(movieSchedule: MovieSchedule) {
		dslContext.insertInto(MOVIE_SCHEDULES)
			.set(movieSchedule.toRecord())
			.onConflict(MOVIE_SCHEDULES.ID)
			.doUpdate()
			.set(movieSchedule.toRecord())
			.execute()
	}

	@Transactional(readOnly = true)
	override fun findById(movieScheduleId: MovieScheduleId): MovieSchedule? {
		return dslContext.selectFrom(MOVIE_SCHEDULES)
			.where(MOVIE_SCHEDULES.ID.eq(movieScheduleId.value))
			.fetchOne { it.toEntity() }
	}

	@Transactional(readOnly = true)
	override fun getMovieSchedules(movieId: MovieId): List<MovieSchedule> {
		return dslContext.selectFrom(MOVIE_SCHEDULES)
			.where(MOVIE_SCHEDULES.MOVIE_ID.eq(movieId.value))
			.orderBy(MOVIE_SCHEDULES.SCHEDULE_DATE.asc())
			.fetch { it.toEntity() }
	}

	private fun MovieSchedule.toRecord() = MovieSchedulesRecord(
		id = id.value,
		scheduleDate = date.value,
		movieId = movieId.value,
		slots = timeSlots.writeAsJSONB()
	)

	private fun MovieSchedulesRecord.toEntity() = MovieSchedule(
		id = MovieScheduleId(id!!),
		date = MovieScheduleDate(scheduleDate!!),
		movieId = MovieId(movieId!!),
		timeSlots = slots!!.readAsObject()
	)
}