package com.demo.moviecatalog.infrastructure

import com.demo.moviecatalog.domain.Movie
import com.demo.moviecatalog.domain.MovieDetails
import com.demo.moviecatalog.domain.OpenMovieRepository
import com.demo.moviecatalog.domain.Rating
import com.demo.type.IMDbId
import com.demo.util.logger
import com.demo.util.readFromJson
import com.fasterxml.jackson.annotation.JsonProperty
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component


@Component
class ExternalOpenMovieRepository(
	@Value("\${moviecatalog.open.movie.api.key}")
	private val apiKey: String,
	@Value("\${moviecatalog.open.movie.api.url}")
	private val serviceUrl: String,
) : OpenMovieRepository {

	private val httpClient = HttpClientBuilder.create()
		.build()

	override fun findByIMBdId(imDbId: IMDbId): Movie? {
		val request = HttpGet("$serviceUrl?apiKey=${apiKey}&i=${imDbId.value}")

		return runCatching { httpClient.execute(request) }
			.map { it.entity.content }
			.map { inputStream -> inputStream.bufferedReader().use { it.readText() } }
			.mapCatching { readFromJson<OpenMovieData>(it) }
			.onFailure { logger.error("Error during fetching movie from external service. $imDbId", it) }
			.map { it.toEntity() }
			.getOrNull()
	}

	private fun OpenMovieData.toEntity() = Movie(
		imdbId = IMDbId(imdbID),
		ratings = ratings.map { Rating(it.source, it.value) },
		details = MovieDetails(
			title = title,
			year = year,
			runtime = runtime,
			genre = genre,
			plot = plot,
			released = released,
			actors = actors,
			poster = poster
		)
	)
}

class OpenMovieData(
	@JsonProperty("Title") val title: String,
	@JsonProperty("Year") val year: Int,
	@JsonProperty("Released") val released: String,
	@JsonProperty("Runtime") val runtime: String,
	@JsonProperty("Genre") val genre: String,
	@JsonProperty("Actors") val actors: String,
	@JsonProperty("Plot") val plot: String,
	@JsonProperty("Poster") val poster: String,
	@JsonProperty("Ratings") val ratings: List<RatingData>,
	val imdbID: String,
)

class RatingData(
	@JsonProperty("Source") val source: String,
	@JsonProperty("Value") val value: String
)