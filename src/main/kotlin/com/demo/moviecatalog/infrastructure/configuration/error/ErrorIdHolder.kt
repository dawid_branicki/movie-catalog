package com.demo.moviecatalog.infrastructure.configuration.error

import com.demo.type.ErrorId
import com.demo.util.threadLocal


object ErrorIdHolder {

	private var errorIdThread by threadLocal<ErrorId?>()

	fun set(errorId: ErrorId) {
		errorIdThread = errorId
	}

	fun clean() {
		errorIdThread = null
	}

	fun getOrNull() = errorIdThread
}