package com.demo.moviecatalog.infrastructure.configuration.swagger

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/")
class MainController {

	@GetMapping
	fun redirectToSwagger(): String {
		return "redirect:/swagger-ui/index.html"
	}
}