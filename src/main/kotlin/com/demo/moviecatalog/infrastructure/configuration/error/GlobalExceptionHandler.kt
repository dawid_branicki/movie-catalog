package com.demo.moviecatalog.infrastructure.configuration.error

import org.springframework.core.annotation.AnnotatedElementUtils
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime
import java.time.ZoneOffset

@RestControllerAdvice
internal class GlobalExceptionHandler : ResponseEntityExceptionHandler() {

	@ExceptionHandler(Exception::class)
	fun handleException(exception: Exception): ResponseEntity<ApiErrorRestDto> {
		val springAnnotation = AnnotatedElementUtils
			.findMergedAnnotation(exception::class.java, ResponseStatus::class.java)
		val timestamp = LocalDateTime.now(ZoneOffset.UTC)
		val status = springAnnotation?.code ?: HttpStatus.INTERNAL_SERVER_ERROR

		val message = when {
			status.is5xxServerError -> "Server error"
			status.is4xxClientError && springAnnotation != null -> exception.message
			else -> "${status.value()} Error"
		}

		if (status.is5xxServerError) {
			logger.error(exception)
		} else {
			logger.warn(exception)
		}

		return ResponseEntity(
			ApiErrorRestDto(
				message = message ?: "Unknown error",
				status = status.value(),
				timestamp = timestamp,
				errorId = ErrorIdHolder.getOrNull()
			), status
		)
	}
}
