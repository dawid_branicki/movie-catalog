package com.demo.moviecatalog.infrastructure.configuration.error

import com.demo.type.ErrorId
import java.time.LocalDateTime

internal class ApiErrorRestDto(
	val message: String,
	val status: Int,
	val timestamp: LocalDateTime,
	val errorId: ErrorId?
)
