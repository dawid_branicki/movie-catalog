package com.demo.moviecatalog.infrastructure.configuration.swagger

import com.demo.moviecatalog.ui.INTERNAL_API_CONTEXT
import com.demo.moviecatalog.ui.PUBLIC_API_CONTEXT
import com.demo.type.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import java.math.BigDecimal
import java.util.*

@Configuration
class SwaggerConfiguration(
	@Value("\${application.version}")
	private val applicationVersion: String
) {

	@Bean
	fun internalApi(): Docket = Docket(DocumentationType.SWAGGER_2)
		.groupName("Internal API")
		.useDefaultResponseMessages(false)
		.select()
		.apis(RequestHandlerSelectors.any())
		.paths(PathSelectors.ant("$INTERNAL_API_CONTEXT/**"))
		.build()
		.apiInfo(apiInfoBuilder())
		.apply(modelSubstitutes)

	@Bean
	fun publicApi(): Docket = Docket(DocumentationType.SWAGGER_2)
		.groupName("Public API")
		.useDefaultResponseMessages(false)
		.select()
		.apis(RequestHandlerSelectors.any())
		.paths(PathSelectors.ant("$PUBLIC_API_CONTEXT/**"))
		.build()
		.apiInfo(apiInfoBuilder())
		.apply(modelSubstitutes)

	private fun apiInfoBuilder() = ApiInfoBuilder()
		.title("Movie Catalog REST API")
		.version(applicationVersion)
		.build()

	private val modelSubstitutes: Docket.() -> Unit get() = {
		directModelSubstitute(MovieId::class.java, UUID::class.java)
		directModelSubstitute(IMDbId::class.java, String::class.java)
		directModelSubstitute(MovieScheduleId::class.java, UUID::class.java)
		directModelSubstitute(MovieTimeSlotId::class.java, String::class.java)
		directModelSubstitute(USD::class.java, BigDecimal::class.java)
		directModelSubstitute(ReviewNote::class.java, Int::class.java)
		directModelSubstitute(ErrorId::class.java, UUID::class.java)
	}
}