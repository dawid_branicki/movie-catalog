package com.demo.moviecatalog.infrastructure.configuration.error

import org.slf4j.MDC
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class ErrorMarkerFilter : OncePerRequestFilter() {

	override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
		val errorId = ErrorId()
		ErrorIdHolder.set(errorId)

		try {
			errorId.markLogs {
				filterChain.doFilter(request, response)
			}
		} finally {
			ErrorIdHolder.clean()
		}
	}

	private fun <T> ErrorId.markLogs(body: () -> T): T {
		MDC.put(ERROR_ID_MARKER, value.toString())
		try {
			return body()
		} finally {
			MDC.remove(ERROR_ID_MARKER)
		}
	}
}

@Configuration
class ErrorMarkerConfiguration {

	@Bean
	fun errorMarkerFilterRegistrationBean() = FilterRegistrationBean<ErrorMarkerFilter>().apply {
		filter = ErrorMarkerFilter()
		order = Ordered.HIGHEST_PRECEDENCE + 1
	}
}

private const val ERROR_ID_MARKER = "errorId"