package com.demo.moviecatalog.infrastructure.configuration

import com.demo.type.IMDbId
import com.demo.type.MovieId
import com.demo.type.ReviewNote
import com.demo.type.USD
import com.demo.util.JacksonUtils
import com.demo.util.TypeDeserializer
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import java.math.BigDecimal
import java.util.*

@Configuration
class JacksonConfiguration {

	@Bean
	@Primary
	fun objectMapper(): ObjectMapper = JacksonUtils.fieldBasedRelaxedMapper()
		.enable(SerializationFeature.INDENT_OUTPUT)
		.setSerializationInclusion(JsonInclude.Include.NON_NULL)
		.registerModule(
			SimpleModule()
				.addDeserializer(USD::class.java, TypeDeserializer(USD::class, BigDecimal::class, ::USD))
				.addDeserializer(MovieId::class.java, TypeDeserializer(MovieId::class, UUID::class, ::MovieId))
				.addDeserializer(ReviewNote::class.java, TypeDeserializer(ReviewNote::class, Int::class, ::ReviewNote))
				.addDeserializer(IMDbId::class.java, TypeDeserializer(IMDbId::class, String::class, ::IMDbId))
		)
}