package com.demo.moviecatalog.ui

import com.demo.moviecatalog.application.MovieScheduleFacade
import com.demo.moviecatalog.application.MovieScheduleRestDto
import com.demo.type.MovieId
import com.demo.type.MovieScheduleId
import com.demo.type.MovieTimeSlotId
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(INTERNAL_API_CONTEXT)
class MovieScheduleInternalController(
	private val movieScheduleFacade: MovieScheduleFacade
) {

	@ApiOperation("Get movie schedules")
	@GetMapping("/movie-schedules/movie/{movieId}")
	fun getMovieSchedules(@PathVariable movieId: MovieId): List<MovieScheduleRestDto> =
		movieScheduleFacade.getMovieSchedules(movieId)

	@ApiOperation("Get movie schedule")
	@GetMapping("/movie-schedules/{movieScheduleId}")
	fun getMovieSchedule(@PathVariable movieScheduleId: MovieScheduleId): MovieScheduleRestDto =
		movieScheduleFacade.findMovieSchedule(movieScheduleId)

	@ApiOperation("Create movie schedule")
	@PostMapping("/movie-schedules")
	fun createMovieSchedule(@RequestBody command: NewMovieScheduleRestCommand): Unit =
		movieScheduleFacade.creteNewMovieSchedule(command)

	@ApiOperation("Add slot time to movie schedule")
	@PostMapping("/movie-schedules/{movieScheduleId}")
	fun addMovieSlotTimeToSchedule(
		@PathVariable movieScheduleId: MovieScheduleId,
		@RequestBody command: AddMovieTimeSlotsRestCommand
	) : Unit = movieScheduleFacade.addMovieTimeSlots(movieScheduleId, command)

	@ApiOperation("Remove slot time from movie schedule")
	@DeleteMapping("/movie-schedules/{movieScheduleId}/time-slot/{slotId}")
	fun removeMovieTimeSlot(
		@PathVariable movieScheduleId: MovieScheduleId,
		@PathVariable slotId: MovieTimeSlotId,
	): Unit = movieScheduleFacade.removeMovieTimeSlot(movieScheduleId, slotId)

}