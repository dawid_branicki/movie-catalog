package com.demo.moviecatalog.ui

private const val API_CONTEXT_PREFIX = "/api/v1/movie-catalog"
const val INTERNAL_API_CONTEXT = "$API_CONTEXT_PREFIX/internal"
const val PUBLIC_API_CONTEXT = "$API_CONTEXT_PREFIX/public"