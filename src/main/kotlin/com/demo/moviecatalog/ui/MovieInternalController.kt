package com.demo.moviecatalog.ui

import com.demo.moviecatalog.application.MovieFacade
import com.demo.moviecatalog.application.MovieRestDto
import com.demo.type.MovieId
import com.demo.util.Page
import com.demo.util.Pager
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(INTERNAL_API_CONTEXT)
internal class MovieInternalController(
	private val movieFacade: MovieFacade
) {

	@ApiOperation("Get all movies")
	@GetMapping("/movies")
	fun getMovies(
		@ApiParam(defaultValue = "1", value = "Page number")
		@RequestParam(required = false, defaultValue = "1") pageNumber: Int,
		@ApiParam(defaultValue = "10", value = "Page size")
		@RequestParam(required = false, defaultValue = "10") pageSize: Int
	): Pager<MovieRestDto> {
		return movieFacade.getMovies(Page(pageNumber, pageSize))
	}

	@ApiOperation("Get movie")
	@GetMapping("/movies/{movieId}")
	fun getMovie(@PathVariable movieId: MovieId): MovieRestDto {
		return movieFacade.getMovie(movieId)
	}

	@ApiOperation("Add new movie from Open Movie API")
	@PostMapping("/movies")
	fun addNewMovie(@RequestBody command: NewMovieRestCommand) {
		return movieFacade.createMovie(command)
	}

	@ApiOperation("Activate movie")
	@PatchMapping("/movies/{movieId}/activate")
	fun activateMovie(@PathVariable movieId: MovieId) {
		return movieFacade.markMovieAsActive(movieId)
	}

	@ApiOperation("Deactivate movie")
	@PatchMapping("/movies/{movieId}/deactivate")
	fun deactivateMovie(@PathVariable movieId: MovieId) {
		return movieFacade.markMoveAsInactive(movieId)
	}

	@ApiOperation("Edit movie")
	@PutMapping("/movies/{movieId}")
	fun updateMovie(@PathVariable movieId: MovieId, @RequestBody command: EditMovieRestCommand) {
		movieFacade.editMovie(movieId, command)
	}
}