package com.demo.moviecatalog.ui

import com.demo.moviecatalog.application.MovieFacade
import com.demo.moviecatalog.application.MovieRestDto
import com.demo.type.MovieId
import com.demo.util.Page
import com.demo.util.Pager
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(PUBLIC_API_CONTEXT)
class MoviePublicController(
	private val movieFacade: MovieFacade
) {

	@ApiOperation("Get all active movies")
	@GetMapping("/movies")
	fun getMovies(
		@ApiParam(defaultValue = "1", value = "Page number")
		@RequestParam(required = false, defaultValue = "1") pageNumber: Int,
		@ApiParam(defaultValue = "10", value = "Page size")
		@RequestParam(required = false, defaultValue = "10") pageSize: Int
	): Pager<MovieRestDto> {
		return movieFacade.getActiveMovies(Page(pageNumber, pageSize))
	}

	@ApiOperation("Get movie")
	@GetMapping("/movies/{movieId}")
	fun getMovie(@PathVariable movieId: MovieId): MovieRestDto {
		return movieFacade.getMovie(movieId)
	}

	@ApiOperation("Add movie review")
	@PutMapping("/movies/{movieId}/review")
	fun addReview(@PathVariable movieId: MovieId, @RequestBody command: AddMovieReviewRestCommand) {
		movieFacade.addMovieReview(movieId, command)
	}
}