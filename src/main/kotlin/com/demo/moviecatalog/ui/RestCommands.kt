package com.demo.moviecatalog.ui

import com.demo.moviecatalog.application.*
import com.demo.type.IMDbId
import com.demo.type.MovieId
import com.demo.type.ReviewNote
import com.demo.type.USD
import java.time.LocalDateTime

class NewMovieRestCommand(override val imdbId: IMDbId) : NewMovieCommand

class EditMovieRestCommand(
	override val regularPrice: USD,
	override val halfPrice: USD
) : EditMovieCommand

class AddMovieReviewRestCommand(override val note: ReviewNote) : AddMovieReviewCommand

class NewMovieScheduleRestCommand(
	override val movieId: MovieId,
	override val year: Int,
	override val month: Int
) : NewMovieScheduleCommand

class AddMovieTimeSlotsRestCommand(
	override val slots: List<MovieTimeSlotRestCommand>
) : AddMovieTimeSlotsCommand

class MovieTimeSlotRestCommand(
	override val start: LocalDateTime,
	override val end: LocalDateTime
) : MovieTimeSlotCommand