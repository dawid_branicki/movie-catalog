package com.demo.moviecatalog

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MovieCatalogApplication

fun main(args: Array<String>) {
	runApplication<MovieCatalogApplication>(*args)
}
