package com.demo.moviecatalog.application

import com.demo.moviecatalog.domain.*
import com.demo.type.IMDbId
import com.demo.type.MovieId
import com.demo.util.Page
import com.demo.util.Pager
import org.springframework.stereotype.Component

@Component
class MovieFacade(
	private val movieRepository: MovieRepository,
	private val openMovieRepository: OpenMovieRepository
) {

	fun getMovies(page: Page): Pager<MovieRestDto> {
		return movieRepository.getMovies(page)
			.map { MovieToMovieRestDtoConverter.convert(it) }
	}

	fun getActiveMovies(page: Page): Pager<MovieRestDto> {
		return movieRepository.getActiveMovies(page)
			.map { MovieToMovieRestDtoConverter.convert(it) }
	}

	fun getMovie(movieId: MovieId): MovieRestDto {
		return movieRepository.findMovie(movieId)
			?.let { MovieToMovieRestDtoConverter.convert(it) }
			?: throw MovieNotFoundException(movieId)
	}

	fun createMovie(command: NewMovieCommand) {
		createMovie(command.imdbId)
	}

	fun createMovie(imdbId: IMDbId) {
		val movie = openMovieRepository.findByIMBdId(imdbId)
			?: throw MovieNotFoundInOpenRepositoryException(imdbId)

		movieRepository.save(movie)
	}

	fun markMovieAsActive(id: MovieId) {
		invokeMovieUpdate(id) {
			markAsActive()
		}
	}

	fun markMoveAsInactive(id: MovieId) {
		invokeMovieUpdate(id) {
			markAsInactive()
		}
	}

	fun editMovie(id: MovieId, editMovieCommand: EditMovieCommand) {
		invokeMovieUpdate(id) {
			changePrice(
				regular = editMovieCommand.regularPrice,
				half = editMovieCommand.halfPrice
			)
		}
	}

	fun addMovieReview(id: MovieId, addMovieReviewCommand: AddMovieReviewCommand) {
		invokeMovieUpdate(id) {
			addReview(addMovieReviewCommand.note)
		}
	}

	private fun invokeMovieUpdate(movieId: MovieId, action: Movie.() -> Unit) {
		movieRepository.findMovie(movieId)
			?.also { action(it) }
			?.also { movieRepository.save(it) }
			?: throw MovieNotFoundException(movieId)
	}
}