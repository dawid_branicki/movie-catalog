package com.demo.moviecatalog.application

import com.demo.moviecatalog.domain.*
import com.demo.type.MovieId
import com.demo.type.MovieScheduleId
import com.demo.type.MovieTimeSlotId
import org.springframework.stereotype.Component

@Component
class MovieScheduleFacade(
	private val movieScheduleRepository: MovieScheduleRepository
) {

	fun creteNewMovieSchedule(command: NewMovieScheduleCommand) {
		val scheduleDate = MovieScheduleDate(year = command.year, month = command.month)
		val schedule = MovieSchedule(
			movieId = command.movieId,
			date = scheduleDate
		)

		movieScheduleRepository.save(schedule)
	}

	fun addMovieTimeSlots(movieScheduleId: MovieScheduleId, command: AddMovieTimeSlotsCommand) {
		val schedule = movieScheduleRepository.findById(movieScheduleId)
			?: throw MovieScheduleNotFoundException(movieScheduleId)

		command.slots
			.map { MovieTimeSlot(SlotDateTime(it.start), SlotDateTime(it.end)) }
			.forEach { schedule[MovieTimeSlotId()] = it }

		movieScheduleRepository.save(schedule)
	}

	fun removeMovieTimeSlot(movieScheduleId: MovieScheduleId, slotId: MovieTimeSlotId) {
		val schedule = movieScheduleRepository.findById(movieScheduleId)
			?: throw MovieScheduleNotFoundException(movieScheduleId)

		schedule.remove(slotId)
		movieScheduleRepository.save(schedule)
	}

	fun findMovieSchedule(movieScheduleId: MovieScheduleId): MovieScheduleRestDto {
		val schedule = movieScheduleRepository.findById(movieScheduleId)
			?: throw MovieScheduleNotFoundException(movieScheduleId)

		return schedule.toMovieScheduleRestDto()
	}

	fun getMovieSchedules(movieId: MovieId): List<MovieScheduleRestDto> {
		return movieScheduleRepository.getMovieSchedules(movieId)
			.map { it.toMovieScheduleRestDto() }
	}

	private fun MovieSchedule.toMovieScheduleRestDto() = MovieScheduleRestDto(
		id = id,
		year = date.value.year,
		month = date.value.monthValue,
		slots = map { it.key to MovieSlotTimeRestDto(it.value.start.value, it.value.end.value) }.toMap()
	)
}