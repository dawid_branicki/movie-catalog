package com.demo.moviecatalog.application

import com.demo.type.IMDbId
import com.demo.type.MovieId
import com.demo.type.ReviewNote
import com.demo.type.USD
import java.time.LocalDateTime

interface NewMovieCommand {
	val imdbId: IMDbId
}

interface EditMovieCommand {
	val regularPrice: USD
	val halfPrice: USD
}

interface AddMovieReviewCommand {
	val note: ReviewNote
}

interface NewMovieScheduleCommand {
	val movieId: MovieId
	val year: Int
	val month: Int
}

interface AddMovieTimeSlotsCommand {
	val slots: List<MovieTimeSlotCommand>
}

interface MovieTimeSlotCommand {
	val start: LocalDateTime
	val end: LocalDateTime
}