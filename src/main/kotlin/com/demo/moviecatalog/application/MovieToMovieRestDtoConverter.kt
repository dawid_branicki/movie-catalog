package com.demo.moviecatalog.application

import com.demo.moviecatalog.domain.Movie

object MovieToMovieRestDtoConverter {

	fun convert(movie: Movie) = with(movie) {
		MovieRestDto(
			id = id,
			title = details.title,
			year = details.year,
			runtime = details.runtime,
			genre =  details.genre,
			plot = details.plot,
			released = details.released,
			actors = details.actors,
			poster = details.poster,
			price = PriceRestDto(price.regular, price.half),
			customerReviewRating = reviewRating.value,
			ratings = ratings.map { RatingRestDto(it.source, it.value) },
			isActive = isActive
		)
	}
}