package com.demo.moviecatalog.application

import com.demo.type.MovieId
import com.demo.type.MovieScheduleId
import com.demo.type.MovieTimeSlotId
import com.demo.type.USD
import com.fasterxml.jackson.annotation.JsonFormat
import java.math.BigDecimal
import java.time.LocalDateTime

class MovieRestDto(
	val id: MovieId,
	val title: String,
	val year: Int,
	val runtime: String,
	val genre: String,
	val plot: String,
	val released: String,
	val actors: String,
	val poster: String,
	val price: PriceRestDto,
	val customerReviewRating: BigDecimal,
	val ratings: List<RatingRestDto>,
	val isActive: Boolean
)

class RatingRestDto(
	val source: String,
	val value: String
)

class PriceRestDto(
	val regular: USD,
	val half: USD
)

class MovieScheduleRestDto(
	val id: MovieScheduleId,
	val year: Int,
	val month: Int,
	val slots: Map<MovieTimeSlotId, MovieSlotTimeRestDto>
)

const val ISO_8061_DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'"

class MovieSlotTimeRestDto(
	@JsonFormat(pattern = ISO_8061_DATETIME_PATTERN, shape = JsonFormat.Shape.STRING)
	val start: LocalDateTime,
	@JsonFormat(pattern = ISO_8061_DATETIME_PATTERN, shape = JsonFormat.Shape.STRING)
	val end: LocalDateTime
)