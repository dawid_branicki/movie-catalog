package com.demo.moviecatalog.application

import com.demo.type.IMDbId
import com.demo.util.logger
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class MovieAutoImporter(
	private val movieFacade: MovieFacade
) {

	/*
	* Alternatively to use @EventListener(ApplicationReadyEvent::class.java) or implement
	* ApplicationRunner interface instead of @PostConstruct
	*/
	@PostConstruct
	fun autoImport() {
		movieListIds.forEach { id ->
			runCatching { movieFacade.createMovie(IMDbId(id)) }
				.onSuccess { logger.info("Movie with imdbId $id has been successfully imported") }
				.onFailure { logger.error("Unable to create movie with imdbId: $id", it) }
		}
	}
}

private val movieListIds = listOf(
	"tt0232500",
	"tt0322259",
	"tt0463985",
	"tt1013752",
	"tt1596343",
	"tt1905041",
	"tt2820852",
	"tt4630562"
)