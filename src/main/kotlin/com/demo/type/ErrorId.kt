package com.demo.type

import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

data class ErrorId(@JsonValue val value: UUID = UUID.randomUUID())
