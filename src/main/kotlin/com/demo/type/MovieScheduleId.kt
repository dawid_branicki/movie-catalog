package com.demo.type

import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

data class MovieScheduleId(@JsonValue val value: UUID) {
	constructor() : this(UUID.randomUUID())
	constructor(value: String) : this(UUID.fromString(value))
}
