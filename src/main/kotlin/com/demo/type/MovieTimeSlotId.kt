package com.demo.type

import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

data class MovieTimeSlotId(@JsonValue val value: String) {
	constructor() : this(UUID.randomUUID().toString())
	constructor(value: UUID) : this(value.toString())
}
