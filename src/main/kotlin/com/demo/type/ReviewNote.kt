package com.demo.type

import com.demo.util.validate
import com.fasterxml.jackson.annotation.JsonValue
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

data class ReviewNote(@JsonValue val value: Int) {
	init {
		validate(value in (1..5)) { ReviewNoteNotInRangeException(value) }
	}

	operator fun plus(value: Long) = this.value + value
}

@ResponseStatus(HttpStatus.BAD_REQUEST)
class ReviewNoteNotInRangeException(currentValue: Int) :
	RuntimeException("Review note must be in range 1-5. Current value equals $currentValue")