package com.demo.type

import com.demo.util.validate
import com.fasterxml.jackson.annotation.JsonValue
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.math.BigDecimal

data class USD(@JsonValue val value: BigDecimal) {

	constructor(value: Double) : this(value.toBigDecimal())

	init {
		validate(value >= BigDecimal.ZERO) { USDLessThanZeroException() }
	}

	companion object {
		val ZERO = USD(BigDecimal.ZERO)
	}
}

@ResponseStatus(HttpStatus.BAD_REQUEST)
class USDLessThanZeroException :
	RuntimeException("USD must be greater or equals zero")