package com.demo.type

import com.fasterxml.jackson.annotation.JsonValue

data class IMDbId(@JsonValue val value: String)
