package com.demo.util

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import org.jooq.JSONB

internal object JacksonUtils {
	fun fieldBasedRelaxedMapper(objectMapper: ObjectMapper = ObjectMapper()): ObjectMapper = objectMapper
		.findAndRegisterModules()
		.setVisibility(
			ObjectMapper().serializationConfig.defaultVisibilityChecker
				.withFieldVisibility(JsonAutoDetect.Visibility.ANY)
				.withGetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withIsGetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withSetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withCreatorVisibility(JsonAutoDetect.Visibility.NONE)
		)
		.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
}

internal val fieldBasedMapper by lazy { JacksonUtils.fieldBasedRelaxedMapper() }

internal inline fun <reified T : Any> JSONB.readAsObject() =
	fieldBasedMapper.readerFor(object : TypeReference<T>() {}).readValue<T>(this.data())

internal inline fun <reified T : Any> T.writeAsJSONB(): JSONB =
	JSONB.valueOf(fieldBasedMapper.writeValueAsString(this))

internal inline fun <reified T : Any> readFromJson(json: String) =
	fieldBasedMapper.readerFor(object : TypeReference<T>() {}).readValue<T>(json)