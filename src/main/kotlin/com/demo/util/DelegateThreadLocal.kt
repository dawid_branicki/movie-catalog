package com.demo.util

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun <T> threadLocal() = DelegateThreadLocal<T>()

class DelegateThreadLocal<T> : ReadWriteProperty<Any?, T> {
	private val threadLocal = ThreadLocal<T>()

	override fun getValue(thisRef: Any?, property: KProperty<*>): T =
		threadLocal.get()

	override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
		if (value == null) {
			threadLocal.remove()
		} else {
			threadLocal.set(value)
		}
	}
}