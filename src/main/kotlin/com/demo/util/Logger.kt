package com.demo.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

internal val allLoggersMap = ConcurrentHashMap<KClass<*>, Logger>()

internal inline val <reified T: Any> T.logger: Logger
	get() = allLoggersMap.getOrPut(T::class) { LoggerFactory.getLogger(T::class.java) }
