package com.demo.util

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import kotlin.reflect.KClass

class TypeDeserializer<T : Any, R : Any>(
	klassType: KClass<T>,
	private val valueType: KClass<R>,
	private val creator: (R) -> T
) : StdDeserializer<T>(klassType.java) {
	override fun deserialize(p: JsonParser, ctxt: DeserializationContext): T? {
		val type = p.readValueAs(valueType.java)
		return creator(type)
	}
}