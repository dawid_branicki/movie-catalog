package com.demo.util

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

class Pager<T : Any>(
	val items: List<T>,
	val totalItems: Int
) {
	init {
		require(totalItems >= 0) { "totalItems must be positive" }
	}

	@Suppress("UNCHECKED_CAST")
	fun <R : Any> map(body: (T) -> R): Pager<R> {
		val mappedItems = items.map(body)
		return Pager(mappedItems, (this as Pager<R>).totalItems)
	}
}

class Page(
	private val pageNumber: Int,
	private val pageSize: Int
) {

	init {
		validate(pageNumber > 0) { IncorrectPageNumberException() }
		validate(pageSize > 0) { IncorrectPageNumberException() }
	}

	val offset get() = (pageNumber - 1) * pageSize
	val limit get() = pageSize

	fun <T : Any> createPager(totalItems: Int, items: List<T>): Pager<T> {
		return Pager(
			items = items,
			totalItems = totalItems
		)
	}
}

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class IncorrectPageNumberException : Exception("Page number and page size must be numbers greater than zero")
